# OpenVPN extended StateLearner

StateLearner is a tool that can learn state machines from implementations using a black-box approach. It makes use of LearnLib for the learning specific algorithms.

This work is a derivative work from Joeri de Ruiter's <a href="https://github.com/jderuiter/statelearner">StateLearner</a> which can be used for OpenVPN implementations.